float x, y, w;
color c1, c2;

void setup() { 
  size(500, 500);
  x = width/2;
  y = height/2;
  w = 64;

  rectMode(CENTER);
}

void draw() {
  if (mouseX < x) {
    c1 = color(255, 120, 36, 175);
    c2 = color(50, 36, 189, 175);
  }
  else {
    c1 = color(#8E1C9B, 175);
    c2 = color(#31E80E, 175);
  }
  dessine(c2, x/2, y);
  dessine(c1, 3*(x/2), y);
}

