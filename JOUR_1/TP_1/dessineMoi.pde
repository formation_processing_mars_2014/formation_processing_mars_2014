void dessine(color c, float _x, float _y) {
  strokeWeight(1);
  stroke(0);
  line(0, _y, 2 * _x, _y);
  line(_x, 0, _x, 2 * _y); 
  stroke(255, 0, 0);
  fill(c);
  strokeWeight(5);
  rect(_x, _y, w, w, 15);
  // ici je décide de peindre ma forme en violet
  fill(#D210E8);
  noStroke();
  ellipse(_x, _y, w/2, w/2);
}

