size(400, 400);
rectMode(CENTER);

int m = width/4;
int l = 25;

// forme simple
point    (m,   m);
ellipse  (m*2, m, 2*l, 2*l);
rect     (m*3, m, 2*l, 2*l);
line     (m-l, m*2, m+l, m*2);

arc      (m*2, m*2, 2*l, 2*l, -HALF_PI, PI);

strokeWeight(4);
quad     ( (m*3)-l, (m*2)-l, (m*3)+l, (m*2)-12, (m*3)+14, (m*2)+14, (m*3)-l, (m*2)+l);
triangle ( m-l, (m*3)+l, m, (m*3)-l, m+l, (m*3)+l);
strokeWeight(1);

int m2 = m*2;
int m3 = m*3;

bezier   (m2-l, m3-l, m2+l, m3-l, m2-l, m3+l, m2+l, m3+l);
noFill();

curve    (m3-2*l, m3+l, m3-l, m3-l, m3+l, m3+l, m3+l, m3-2*l); 
