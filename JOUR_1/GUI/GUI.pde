void draw() {

  if (overRect(20, 20, 20, 20) == true) {
    background(255);
  }
  else {
    background(0);
  }

  fill(255, 0, 0);
  rect(20, 20, 20, 20);
}


boolean overRect(int x, int y, int width, int height) {
  if (mouseX >= x && mouseX <= x+width && 
    mouseY >= y && mouseY <= y+height) {
    return true;
  } 
  else {
    return false;
  }
}

