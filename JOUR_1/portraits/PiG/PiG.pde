int taille = 30;
/*
blablabla
 
 */
void setup() {
  size(800, 300);
  background(255);
  rectMode(CENTER);
  float pX = width/2;
  float pY = height/2; 

  dessineMoiUneForme(pX, pY, 60);
  dessineMoiUneForme(pX / 2, pY / 4, 110);
  save("monImage.jpg");
  dessineMoiUneForme(pX * (2/3), pY +(pY/3), 110);
  dessineMoiUneForme(550, 200, 35);
  
}

void dessineMoiUneForme(float x, float y, int _taille) {
  fill(255, 0, 125, 50);
  strokeWeight(3);
  rect(x, y, _taille, _taille, 5);
  rect(x, y, _taille * (2/3), _taille* (2/3), 5);
  rect(x, y, _taille/3, _taille/3, 5);
  fill(255, 50, 0, 70);
  ellipse(x - _taille/3, y - taille/5, _taille/5, _taille/5);
  ellipse(x + _taille/3, y - taille/5, _taille/5, _taille/5);
  rect(x, y + _taille/3, _taille /2, _taille/8);
}

