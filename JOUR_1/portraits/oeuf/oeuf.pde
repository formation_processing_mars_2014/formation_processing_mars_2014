float centreX;
float centreY;
float directionX = 1;
float directionY = 1;
float zoom = 4;
float vitesse = 0.8;

void setup() {
  size(600, 600);
  rectMode(CENTER);
  centreX = random(width);
  centreY = random(height);
}

void draw() {
  background(255);
  // j'appelle la fonction que j'ai crée pour le mouvement
  bouge();


  pushMatrix();
  translate(centreX, centreY);
  scale(zoom);
  //zoreilles
  ellipse(- 25, - 5, 20, 20);
  ellipse(25, - 5, 20, 20);
  //tete

  fill(255);
  ellipse(0, 0, 50, 100);
  //lunettes
  fill(123);
  rect(- 15, -10, 20, 10);
  rect(15, -10, 20, 10);
  //line(centreX - 10, centreY -20, centreX + 10, centreY - 20);

  fill(50);
  // barbe
  arc(0, 15, 45, 68, radians(0), radians(180));
  fill(255);
  // nez
  rect(0, 5, 10, 25);
  fill(255);
  rect(0, 30, 30, 5);
  //cheveux
  for(int i=0; i<20; i++){
    float [] posX = {-20, -15, -10, -5, 0, 5, 10, 15, 20};
    float [] posY = {-30, -20, -25};
    float longueur = posY[(int)random(3)];
    float position = posX[(int)random(9)];
  line(position, longueur , position, longueur-5);
  }
  popMatrix();
}

void bouge() {
  if (centreX > width) { 
    centreX = width;
    directionX *= -1;
  }
  if (centreX < 0) {
    centreX = 0;
    directionX *= -1;
  }
  else {
    centreX += vitesse * directionX;
  }

  if (centreY > height) { 
    centreY = height;
    directionY *= -1;
  }
  if (centreY < 0) {
    centreY = 0;
    directionY *= -1;
  }
  else {
    centreY += vitesse * directionY;
  }
}

