void setup() {
  size(640, 480);
  smooth();
  noCursor();
}
void draw() {
  background(#DE3163);
  afficheForme(width/2, height/2, 150);
}
void afficheForme(float posX, float posY, float taille) {
  posX++;
  strokeWeight(3);
  fill(#FFF300);
  stroke(0);
  ellipse(posX, posY, taille+5, taille+5);
  fill(0);
  stroke(#FFF300);
  arc(posX, posY, taille, taille, radians(180), radians(240));
  arc(posX, posY, taille, taille, radians(300), radians(360));
  arc(posX, posY, taille, taille, radians(60),  radians(120));
  strokeWeight(8);
  ellipse(posX, posY, taille/6, taille/6);
}

