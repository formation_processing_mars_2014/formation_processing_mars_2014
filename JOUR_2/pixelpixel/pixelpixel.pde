// On déclare une variable de type PImage. une class.
PImage keaton;
color c;

void setup() {
  size(240, 320);
  noStroke();

  // On charge l'image en mémoire
  keaton = loadImage("keaton.jpg");

  // On redimensionne l'image
  keaton.resize(240, 320);
} 
void draw() {

  // Sinon on affiche l'image 
  image(keaton, 0, 0);

  // et on récupère la valeur colorimétrique du pixel à l'emplacement de la souris
  if (mouseY < height/2) {
    c = get(mouseX, mouseY);
    // cette valeur nous sert à peindre l'interieur d'un rectangle
    fill(c);
    rect(0, height/2, width, height/2);
    fill(255);
    text(hex(c), 160, 300);
  }
  else {
    fill(c);
    rect(0, height/2, width, height/2);
    fill(255);
    text(hex(c), 160, 300);
  }
}

