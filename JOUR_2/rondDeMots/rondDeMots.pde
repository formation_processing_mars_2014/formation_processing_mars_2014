import java.util.Calendar;

float r=0;

void setup() {
  size(400, 400);
  background(0);
  fill(255, 60);
}

void draw() {
  pushMatrix();
  translate(mouseX, mouseY);
  rotate(r);
  text("Pangolin", 0, 0);
  popMatrix();

  r+=0.02;

  if (keyPressed) {
    if (key == 's') {
      saveFrame("OeuvredArtMajeure"+timestamp()+".png");
    }
  }
}

String timestamp() {                     //  ----- A method with return (here a String) to catch the time and the date a which you save your file
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}
