/*@pjs preload="fond.jpg";*/
boolean dessine;
// je crée une varaiblede type image qui contiendra le fond d'écran et ses mises à jour
PImage fond;

void setup() {
  size(400, 400);
  fond = get();
  noStroke();
}

void draw() {
  //le fond d'écran affiche en permanence l'image 'fond'
  background(fond);
  fill(0, 150);
  //on affiche continuellement une ellipse à la position de la souris
  ellipse(mouseX, mouseY, 11, 11);


  if (dessine) {
    fond = get();
  }

  // je vérifie si le bouton de la souris est enfoncé
  // auquel cas la valeur de 'dessine', change.
  if (mousePressed) {
    dessine = true;
  }
  else {
    dessine = false;
  }

  if (keyPressed) {
    if (key == ' ') {
      background(200);
      fond = get();
    }
    if (key == 's') {
      save("image.jpg");
    }
  }
}

