import java.util.Calendar;
float x, y;

void setup() {
  size(400, 400);
  smooth();
}

void draw() {
  background(255);
  x = mouseX;
  y = mouseY;

  for (int i = 0; i < width; i = i + 15) {
    for (int j=0; j < height; j = j + 15) {
      line(i, y, x, j);
    }
  }

  if (keyPressed) {
    if (key =='s') {
      saveFrame("voila"+timestamp()+".png");
    }
  }
}

String timestamp() {                     //  ----- A method with return (here a String) to catch the time and the date a which you save your file
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}

