String texte [];      //pour récupérer le texte à partir du texte
String phrases [];    //pour récupérer plus tard, chacune des phrases terminées par un point
int y = 20;

void setup() {
  size(700, 400);
  background(0);
  texte = loadStrings("Ponge.txt");
  // initialisation du tableau de String "phrases"
  phrases = new String [0];

  //on parcourt le tableau de texte d'ou on extrait les phrases
  for (int i=0; i<texte.length; i++) {
    String temp[] = splitTokens(texte[i], ".,");
    for (int j=0; j<temp.length; j++) {
      phrases = append(phrases, temp[j]);
    }
  }
  //on enleve la dernière phrase qui n'est autre qu'un blanc
  phrases = subset(phrases, 0, phrases.length-1);

  for (int i=0; i<phrases.length; i++) {
    y += (height - 40)/phrases.length;
    if (phrases[i].contains("huître"))
      fill(255, 0, 0);
    else
      fill(255);

    text(trim(phrases[i])+".", 20, y);
  }
}

