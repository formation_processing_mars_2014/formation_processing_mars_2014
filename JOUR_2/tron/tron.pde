
void setup() {
  size(640, 480);
  smooth();
  noCursor();
}

void draw() {
  background(255);
  float x = mouseX; 
  float y = mouseY; 
  // une boucle pour l'affichage de plusieurs ligne controlées par la position de la souris
  // on s'arrête à 96 car on multiplie le résultat par 5, donc 96 * 5 = 480
  for(int i = 0; i < 96 ;i++) {
    //strokeWeight(i/20f);
    line(x, y, 0, i*5); 
    line(width, i*5, x, y);
  }
}

