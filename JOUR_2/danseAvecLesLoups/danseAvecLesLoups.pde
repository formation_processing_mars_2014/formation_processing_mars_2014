
int nb = 50;    // nbr de bêtes
float x[] = new float [nb];
float y[] = new float [nb];
float xoff = 0.0;
float yoff = 0.0;

int choix = 1; // si 0 alors random, si 1 alors noise

void setup() {
  size(800, 500);
  smooth();
  strokeWeight(3);
  noFill();
  noiseDetail(10, 0.35);
  
  for (int i=0; i<nb; i++) {

    switch(choix) {
    // version random
    case 0:
      x[i] = random(width);
      y[i] = random(height);
      break;
    // version noise
    case 1:
      if ((int)random(2) > 0) {
        xoff += 0.5;
      }
      else {
        xoff -= 0.5;
      }
      if ((int)random(2) > 0) {
        yoff += 0.5;
      }
      else {
        yoff -= 0.5;
      }
      x[i] = noise(xoff) * width;
      y[i] = noise(yoff) * height;
      break;
    }
  }
}

void draw() {
  background(255);
  for (int i=0; i<nb; i++) {

    // calcul la position en radians d'un point par rapport aux coordonnées de la souris
    float angle = atan2(mouseY-y[i], mouseX-x[i]);

    // on mémorise les transformation de matrice
    pushMatrix();
    // on déplace la matrice
    translate(x[i], y[i]);
    rotate(angle);
    stroke(0, 175);
    // la tête de loup
    triangle(-20, -8, 20, 0, -20, 8);
    ellipse(-8, 6, 5, 5);
    ellipse(-8, -6, 5, 5);
    popMatrix();
  }
}

