/*
   Calculates the remainder when one number is divided by another.
   For example, when 52.1 is divided by 10, the divisor (10) goes into 
   the dividend (52.1) five times (5 * 10 == 50),
   and there is a remainder of 2.1 (52.1 - 50 == 2.1). 
   Thus, 52.1 % 10 produces 2.1.
*/


void draw() {
  float x = frameCount % width;
  println(frameCount+"   "+x);
  ellipse(x, 50, 40, 40);
}

