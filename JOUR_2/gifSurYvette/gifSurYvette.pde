import gifAnimation.*;
int nb = 9;
PImage [] anim = new PImage[nb];
GifMaker gifExport;
int frame = 0;

void setup() {
  size(640, 480);
  frameRate(12);
  for (int i = 0; i<9; i++) {
    anim[i] = loadImage("00"+i+".PNG");
  }
  gifExport = new GifMaker(this, "surYvette.gif");
  gifExport.setRepeat(0);             // make it an "endless" animation
  gifExport.setQuality(5);
}

void draw() {
  background(0);
  frame = (frame + 1) % nb;
  image(anim[frame], 0, 0);

  gifExport.setDelay(1);
  gifExport.addFrame();
}

void mousePressed() {
  gifExport.finish();                 // write file
}

