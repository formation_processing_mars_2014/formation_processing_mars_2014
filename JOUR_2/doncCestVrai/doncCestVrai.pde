String [] blabla;

void setup() {
  size(600, 400);
  background(255);
  textSize(24);
  textAlign(CENTER, CENTER);
  blabla = loadStrings("ribery");
  for (int i=0; i<blabla.length; i++) {
    String mots[] = splitTokens(blabla[i], " ");
    for (int j=0; j<mots.length; j++) {
      char[]lettres = mots[j].toCharArray();
      for (int k=0; k<lettres.length; k++) {
        if (lettres[k] == 'e') {
          fill(#110BDE);
        }
        else {
          fill(0);
        }

        pushMatrix();
        translate(15+(k*15), 40+j*40);
        if (lettres[k] == 'a') {
          rotate(radians(180));
          translate(0, -12);
        }
        text(lettres[k], 0, 0);
        popMatrix();
      }
    }
  }
}

