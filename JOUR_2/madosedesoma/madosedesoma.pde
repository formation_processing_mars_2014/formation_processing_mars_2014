
size(1200, 650);

float R [] = {
  HALF_PI, PI, TWO_PI
};
randomSeed(5);

ellipseMode(CENTER);
imageMode(CENTER);
background(255);
smooth();
stroke(0, 150);
float pif = 0;

noFill();
float another = 0;

for (int i=0; i<width; i+=50) {
  for (int j=0; j<width; j+=15) {
    pif = random(i, j);
    another += 0.01;
    float hop = random(height/4);
    float hip = random(width/4);
    float trig = map(sin(another), 0, 1, 0, 3*width/4);
    float trog = map(cos(another), 0, 1, 0, 6*height/4);
    beginShape();
    vertex(-200, -200);

    bezierVertex(0, 0, hip, hop, trig, trog);
    //    line(i, pif,j, height);
    endShape();
  }
}

saveFrame("carte.jpg");

