class Forme {
  float x, y, taille, rot, zoom, a;

  Forme(float _x, float _y, float _taille) {
    x = _x;
    y = _y;
    taille = _taille;
  }

  void affiche() {
    pushMatrix();
    translate(x, y);
    rot+=0.1;
    rotate(rot);
    scale(zoom);
    rect(0, 0, taille, taille);
    popMatrix();
    println(rot);
  }

  void mouvement() {
    rot+=0.1;
    //--------------------
//    if (zoom > 5) zoom = 0;
//    else zoom+= 0.05;
    zoom = map(sin(a), -1, 1, 0, 10);
    a += 0.03;
  }
}

