Lune [] lunes = new Lune[10];
int choix = 0;

void setup() {
  size(1000, 800);
  fill(255);
  for (int i=0; i<lunes.length; i++) {
    lunes[i] = new Lune(i*100);
  }
}

void draw() {
  background(0);
  if (frameCount % 60 == 0) {
    choix++;
  }
  if (choix >= lunes.length) {
    choix = 0;
  }
  lunes[choix].affiche();
}

