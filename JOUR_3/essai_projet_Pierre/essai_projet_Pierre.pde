import processing.video.*;
import java.util.Calendar;

Capture cam;
PImage [] anim = new PImage [0];
int frame = 0;

void setup() {
  size(640, 240);

  String[] cameras = Capture.list();

  if (cameras == null) {
    println("Failed to retrieve the list of available cameras, will try the default...");
    cam = new Capture(this, 320, 240);
  } 
  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } 
  else {
    println("Available cameras:");
    for (int i = 0; i < cameras.length; i++) {
      println(cameras[i]);
    }
    cam = new Capture(this, cameras[4]);
    // Or, the settings can be defined based on the text in the list
    //cam = new Capture(this, 640, 480, "Built-in iSight", 30);

    // Start capturing the images from the camera
    cam.start();
  }
}

void draw() {
  if (cam.available() == true) {
    cam.read();
  }
  image(cam, 0, 0);
  if (keyPressed) {
    PImage m = createImage(640, 480, RGB);
    m = get();
    anim = (PImage[])append(anim, m);
    //save("pic"+timestamp()+".png");
  }
  if (anim.length>0) {
    frame = (frame+1) % anim.length;
    for (int i=0; i<anim.length; i++) {
      image(anim[frame], 320, 0);
    }
    println(anim.length);
  }
  // The following does the same as the above image() line, but 
  // is faster when just drawing the image without any additional 
  // resizing, transformations, or tint.
  //set(0, 0, cam);
}

String timestamp() {                     //  ----- A method with return (here a String) to catch the time and the date a which you save your file
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}

