import processing.video.*;
import java.util.Calendar;

Capture cam;
PGraphics pg;
PGraphics photo;

void setup() {
  size(640, 480, P2D);
  pg = createGraphics(640, 480);

  String[] cameras = Capture.list();

  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } 
  else {
    println("Available cameras:");
    for (int i = 0; i < cameras.length; i++) {
      println(cameras[i]);
    }
  }
  // The camera can be initialized directly using an element
  // from the array returned by list():
  cam = new Capture(this, cameras[2]);
  cam.start();
}

void draw() {
  pg.beginDraw();
  pg.noStroke();
  pg.fill(0, 150);
  if (mousePressed) {
    pg.ellipse(mouseX, mouseY, 18, 18);
  }
  pg.endDraw();

  if (cam.available() == true) {
    cam.read();
  }
  image(cam, 0, 0, 640, 480);
  image(pg, 0, 0);

  if (keyPressed) {
    if (key == 's') {
      saveFrame("face"+timestamp()+".png");
    }
  }
}

String timestamp() {                     //  ----- A method with return (here a String) to catch the time and the date a which you save your file
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}

