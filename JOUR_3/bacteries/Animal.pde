class Animal {
  float posX, posY, w, h;
  float noiseX, noiseY;
  int age = 0;
  boolean dead = false;

  Animal(float _posX, float _posY, float _w, float _h) {
    posX = _posX;
    posY = _posY;
    w = _w;
    h = _h;
  }

  void vivre() {
    fill(255, 70);
    ellipse(posX, posY, w, h);
    age++;
    //println(age);
  }
  
  void seDeplacer() {

    noiseX += random(-2, 2);
    noiseY += random(-3, 5);
    noiseDetail(5, 0.2);
    //noiseSeed(int(random(20)));

    if ((int)random(2) > 0) {
      posX += noise(noiseX) ;
    }
    else {
      posX -= noise(noiseX) ;
    }
    if ((int)random(2) > 0) {
      posY -= noise(noiseY);
    }
    else {
      posY += noise(noiseY);
    }
  }

  void seNourrir() {
  }

  void seReproduire() {
  }

  void mourrir() {
    if (posX > width || posX < 0 || posY > height || posY < 0)
      dead = true;
    if (age >= 278) {
      if (random(50)>47)
        dead = true;
    }
  }
}

