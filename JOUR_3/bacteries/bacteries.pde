ArrayList <Animal> troupeau = null;

void setup() {
  size(640, 480);
  noStroke();
  troupeau = new ArrayList<Animal>();
}

void draw() {
  background(0);
  if (mousePressed) {
    int nb = (int)random(50);
    for (int i = 0; i < nb; i++) {
      troupeau.add(new Animal(mouseX, mouseY, 5, 5));
    }
  }

  if (troupeau != null) {
    for (int i = 0; i < troupeau.size(); i++) {
      Animal teta = troupeau.get(i);

      teta.seDeplacer();
      teta.vivre();
      teta.mourrir();
      if (teta.dead) {
        troupeau.remove(i);
      }
    }
  }
}

