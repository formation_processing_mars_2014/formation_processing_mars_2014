Balle balle;
Raquette r1, r2;
boolean start = false;

void setup() {
  size(640, 480);
  rectMode(CENTER);
  balle = new Balle();
  r1 = new Raquette(40);
  r2 = new Raquette(600);
}

void draw() {
  background(0);
  if (keyPressed) {
    start = true;
  }
  if (start) {
    balle.bouge();
    balle.affiche();
    r1.bouge('z', 'x');
    r1.affiche();
    r2.bouge('u', 'n');
    r2.affiche();

    if (balle.x > (r2.x - r2.w/2)) {
      if (balle.y < (r2.y + r2.h/2) && balle.y > (r2.y - r2.h/2)) {
        balle.direction = -balle.direction;
      }
    }

    if (balle.x < (r1.x + r2.w/2)) {
      if (balle.y < (r1.y + r1.h/2) && balle.y > (r1.y - r1.h/2)) {
        balle.direction = -balle.direction;
      }
    }
  }
}

boolean overCircleRaquetteG(float x, float y, float diameter) {
  float disX = x - mouseX;
  float disY = y - mouseY;
  if (sqrt(sq(disX) + sq(disY)) < diameter/2 ) {
    return true;
  } 
  else {
    return false;
  }
}

