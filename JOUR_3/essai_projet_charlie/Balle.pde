class Balle {
  float x, y, w, vitesse, direction;

  Balle() {
    x = random(width);
    y = random(height);
    w = 15;
    vitesse = random(2, 6);
    direction = 1;
  }

  void bouge() {
    x+= vitesse * direction;
  }

  void affiche() {
    ellipse(x, y, w, w);
  }
}

